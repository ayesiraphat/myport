import  React,{Component}from 'react'
import './App.css';
import {Layout,HeaderRow,Textfield,Content,Drawer,Header,Navigation,} from 'react-mdl'
import {Link} from  'react-router-dom'
import Main from './component/main'

class App extends Component {
  render() {
    return (
        <div className="demo-big-content">
            <Layout>
                <Header title="MY WEB" scroll className="header-color">
                    <Navigation>
                        <Link style={{color:'black'}} to="/">Home</Link>
                        <Link style={{color:'black'}}to="/aboutme">About me</Link>
                        <Link style={{color:'black'}}to="/activity">Activity</Link>
                        <Link style={{color:'black'}}to="/contact">Contact</Link>
                    </Navigation>
                </Header>
                <Drawer title="SIRAPHAT  PURANAWONG">
                    <Navigation>
                        <Link to="/">Home</Link>
                        <Link to="/aboutme">About me</Link>
                        <Link to="/activity">Activity</Link>
                        <Link to="/contact">Contact</Link>
                    </Navigation>
                </Drawer>
                <Content>
                    <div className="page-content" />
                    <Main/>
                </Content>

            </Layout>
        </div>
    );
  }
}

export default App;
