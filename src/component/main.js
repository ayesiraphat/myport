import React  from 'react';
import {Switch,Route } from 'react-router-dom';
import Home from './home'
import Contact from './contact'
import Activity from  './activity'
import Aboutme from './aboutme'

const Main = () => (
    <Switch>
        <Route exact path="/aboutme" component={ Aboutme}/>
        <Route exact path="/activity" component={Activity}/>
        <Route exact path="/contact" component={Contact}/>
        <Route exact path="/" component={Home}/>
    </Switch>
)
export default Main ;