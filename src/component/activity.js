import React,{Component} from 'react'
import {Tab, Tabs, CardActions, Card, CardTitle, Grid, Cell, CardText, Button, Navigation} from 'react-mdl'
import {Link} from "react-router-dom";

class Activity extends Component{
    constructor(props) {
        super(props);
        this.state = { activeTab: 0 };
    }
    toggleCategories(){
        if(this.state.activeTab === 0){
            return(
                <div className="activity-grid">
                    <div className="banner-text">
                        <h3>Picture of Colors sport</h3>
                    </div>
                    {/*1*/}
                <Card shadow={0} style={{width: '256px', height: '256px', background: 'url(https://scontent.fbkk1-2.fna.fbcdn.net/v/t1.0-9/14079963_629304897236679_2640516564690468546_n.jpg?_nc_cat=110&_nc_eui2=AeEcIwcHpBNivsWTE4hXFTPB9l_kT-xYYD60n2cIeS2-lUFfl33J13H6UVjpt7yYx1JV6D5cEEyAfVr_Dko-rAISzw1s715ziG2nf0dtDJ3nCw&_nc_ht=scontent.fbkk1-2.fna&oh=a54f8e9545bea7b71b54ceb43ef8a7fd&oe=5C41974B) center / cover', marginRight: '10px'}}>
        <CardTitle expand />
            <CardActions style={{height: '52px', padding: '16px', background: 'rgba(0,0,0,0.2)'}}>
        <span style={{color: '#fff', fontSize: '14px', fontWeight: '500'}}>
            Image.jpg
            </span>
        </CardActions>
        </Card>
                    {/*1*/}
                    <Card shadow={0} style={{width: '256px', height: '256px', background: 'url(https://scontent.fbkk1-2.fna.fbcdn.net/v/t31.0-8/23509444_1640616205988591_24523292461625519_o.jpg?_nc_cat=107&_nc_eui2=AeEbn5KKUnt26ArrVO8wufZhr7mmEHDWCQtOO9dKBVj4TTn4TZsufhh1Gz3zCzr8ApYH9NL2rFHNMqrAJmGVCnqRt6f_KBwb3hbHOdG5veG94g&_nc_ht=scontent.fbkk1-2.fna&oh=7d532ae659fd55ce3990d648e7d369cc&oe=5C4B5E5D) center / cover', marginRight: '10px'}}>
                        <CardTitle expand />
                        <CardActions style={{height: '52px', padding: '16px', background: 'rgba(0,0,0,0.2)'}}>
        <span style={{color: '#fff', fontSize: '14px', fontWeight: '500'}}>
            Image.jpg
            </span>
                        </CardActions>
                    </Card>
                    {/*1*/}
                    <Card shadow={0} style={{width: '256px', height: '256px', background: 'url(https://scontent.fbkk1-2.fna.fbcdn.net/v/t31.0-8/23511326_1640618725988339_2202922420133913756_o.jpg?_nc_cat=106&_nc_eui2=AeGMrajCbbaCUo0wlbcSTbQ3-s-jWW5JDTQ0-NPlajiOgxuEd6krhBqPO5hBoxJXH3MyIBARnjKNmBP14_9cuKnjAMjTRSiyEL8eWO-CR4m_ug&_nc_ht=scontent.fbkk1-2.fna&oh=5e6d51a2ee47e60e5698bd5073000999&oe=5C79BC3F) center / cover', marginRight: '10px'}}>
                        <CardTitle expand />
                        <CardActions style={{height: '52px', padding: '16px', background: 'rgba(0,0,0,0.2)'}}>
        <span style={{color: '#fff', fontSize: '14px', fontWeight: '500'}}>
            Image.jpg
            </span>
                        </CardActions>
                    </Card>
                    {/*1*/}
                    <Card shadow={0} style={{width: '256px', height: '256px', background: 'url(https://scontent.fbkk1-3.fna.fbcdn.net/v/t31.0-8/23511183_1640616882655190_6174112070630078772_o.jpg?_nc_cat=109&_nc_eui2=AeHe3AF6nqNEtPESn3pFVwIsRHbSHWWx3sIMV4GtqOLhhqe6-nlYLyPOaZ-fWESx2dPzAnj_xFa4j6LFMhssjKG8JyIzzKnksLtYsiE_ccL0Xg&_nc_ht=scontent.fbkk1-3.fna&oh=2769d07858407c428dc36f75cf6e533a&oe=5C7A1655) center / cover', marginRight: '10px'}}>
                        <CardTitle expand />
                        <CardActions style={{height: '52px', padding: '16px', background: 'rgba(0,0,0,0.2)'}}>
        <span style={{color: '#fff', fontSize: '14px', fontWeight: '500'}}>
            Image.jpg
            </span>
                        </CardActions>
                    </Card>

                </div>





            )
        }
        else if(this.state.activeTab === 1) {
            return (
                <div className="activity-grid">
                    <div className="banner-text">
                        <h3>Picture of Thai language Day</h3>
                    </div>
                    {/*1*/}
                    <Card shadow={0} style={{width: '256px', height: '256px', background: 'url(https://scontent.fbkk1-2.fna.fbcdn.net/v/t1.0-9/13886286_620392834794552_7458341347932719487_n.jpg?_nc_cat=106&_nc_eui2=AeEhh5_M2wJ60wgdIM3F0Lx1KexEywxFfsA10JaPVrXHaKni58gFOhtFEuMVOgZIsh4smPfG4UkFnhL8HDZ77Ld5t7TCSwfK-SIxdMfaTd6tRA&_nc_ht=scontent.fbkk1-2.fna&oh=588ff39efb745b33cf8cab0cffc870c6&oe=5C88C63C) center / cover', marginRight: '10px'}}>
                        <CardTitle expand />
                        <CardActions style={{height: '52px', padding: '16px', background: 'rgba(0,0,0,0.2)'}}>
        <span style={{color: '#fff', fontSize: '14px', fontWeight: '500'}}>
            Image.jpg
            </span>
                        </CardActions>
                    </Card>
                    {/*1*/}
                    <Card shadow={0} style={{width: '256px', height: '256px', background: 'url(https://scontent.fbkk1-1.fna.fbcdn.net/v/t1.0-9/13876242_753164864826075_6968138199295990625_n.jpg?_nc_cat=102&_nc_eui2=AeHofxH5YkgDz2Mvqvo5IXdK5YiMwyUdwdzQDpJS0bE7fOCSn6z0ZwK61-59ZO4YPP8LmPZp_wUEb615UwEnr4fUsxmEUdyEGvn-i6UNRQv-Hg&_nc_ht=scontent.fbkk1-1.fna&oh=8344c6abda0cfb77164be3f9bf140a54&oe=5C88C2F7) center / cover', marginRight: '10px'}}>
                        <CardTitle expand />
                        <CardActions style={{height: '52px', padding: '16px', background: 'rgba(0,0,0,0.2)'}}>
        <span style={{color: '#fff', fontSize: '14px', fontWeight: '500'}}>
            Image.jpg
            </span>
                        </CardActions>
                    </Card>
                    {/*1*/}
                    <Card shadow={0} style={{width: '256px', height: '256px', background: 'url(http://www.thailandessential.com/wp-content/uploads/2016/04/Thai-Language-Consonants.png) center / cover', marginRight: '10px'}}>
                        <CardTitle expand />
                        <CardActions style={{height: '52px', padding: '16px', background: 'rgba(0,0,0,0.2)'}}>
        <span style={{color: '#fff', fontSize: '14px', fontWeight: '500'}}>
            Image.jpg
            </span>
                        </CardActions>
                    </Card>

                </div>
            )
        }else if(this.state.activeTab === 2) {
            return (
                <div className="activity-grid">
                    <div className="banner-text">
                        <h3>Picture of Lab-Sci</h3>
                    </div>
                    {/*1*/}
                    <Card shadow={0} style={{width: '256px', height: '256px', background: 'url(https://scontent.fbkk1-1.fna.fbcdn.net/v/t1.0-9/20258025_10210154840613434_6878357570070053749_n.jpg?_nc_cat=108&_nc_eui2=AeHipQX1vk6HBbT3Jf1uxa2_PKGBSL6VoDRDDtPWM-Fnbjgx2ym3_svHNyq4WDdt9e07tIzW069ag2iAfDksQFtaMnanKk24QCVO0RbBGgGekQ&_nc_ht=scontent.fbkk1-1.fna&oh=65fdec3cd1521d69db7e0438a12e5922&oe=5C4E45A8) center / cover', marginRight: '10px'}}>
                        <CardTitle expand />
                        <CardActions style={{height: '52px', padding: '16px', background: 'rgba(0,0,0,0.2)'}}>
        <span style={{color: '#fff', fontSize: '14px', fontWeight: '500'}}>
            Image.jpg
            </span>
                        </CardActions>
                    </Card>
                    {/*1*/}
                    <Card shadow={0} style={{width: '256px', height: '256px', background: 'url(https://scontent.fbkk1-2.fna.fbcdn.net/v/t31.0-8/20247556_10210154755611309_2100448957124068622_o.jpg?_nc_cat=110&_nc_eui2=AeHPrINO0cJ7K_PHKsMBXwID8a0xNTS-v1SpodYTygf4o4d2jEQKap-MLCwbTVJxE6feVZGJZU9z62fZ1-uX0qhZE_CeXffNqljDE_uprCl97w&_nc_ht=scontent.fbkk1-2.fna&oh=df36535386d8315b4b386710f1854124&oe=5C4835B0) center / cover', marginRight: '10px'}}>
                        <CardTitle expand />
                        <CardActions style={{height: '52px', padding: '16px', background: 'rgba(0,0,0,0.2)'}}>
        <span style={{color: '#fff', fontSize: '14px', fontWeight: '500'}}>
            Image.jpg
            </span>
                        </CardActions>
                    </Card>
                    {/*1*/}
                    <Card shadow={0} style={{width: '256px', height: '256px', background: 'url(https://scontent.fbkk1-2.fna.fbcdn.net/v/t31.0-8/20287071_10210154765731562_923382252108911564_o.jpg?_nc_cat=110&_nc_eui2=AeExD1lIsPsT0M6InjO11st8A6fdAgM6Qe52O9nraoRWlqNgzzwrKdVtM14y1bI3FmV4b__G64jP-1M8_Gw3X6oLBc9sF7I5DB7jo2j6b6j_OQ&_nc_ht=scontent.fbkk1-2.fna&oh=608e1302f776165dd32d66c4f8e7b053&oe=5C3C1A90) center / cover', marginRight: '10px'}}>
                        <CardTitle expand />
                        <CardActions style={{height: '52px', padding: '16px', background: 'rgba(0,0,0,0.2)'}}>
        <span style={{color: '#fff', fontSize: '14px', fontWeight: '500'}}>
            Image.jpg
            </span>
                        </CardActions>
                    </Card>
                    {/*1*/}
                    <Card shadow={0} style={{width: '256px', height: '256px', background: 'url(https://scontent.fbkk1-1.fna.fbcdn.net/v/t1.0-9/20258324_10210164420372922_2139119288730575645_n.jpg?_nc_cat=108&_nc_eui2=AeFhh--OzR05mzymDlKR-suO3kGaCTZJSJu9yT7XSf_tIjTA5OBWfUehFprDGv0IEx7YTpeP4_oYVcDJLWD_bR5-s-TEpnkd_fX4JldTos5llw&_nc_ht=scontent.fbkk1-1.fna&oh=8a6996abaf58875caccdf9802586980f&oe=5C4F8B06) center / cover', marginRight: '10px'}}>
                        <CardTitle expand />
                        <CardActions style={{height: '52px', padding: '16px', background: 'rgba(0,0,0,0.2)'}}>
        <span style={{color: '#fff', fontSize: '14px', fontWeight: '500'}}>
            Image.jpg
            </span>
                        </CardActions>
                    </Card>

                </div>
            )
        }
    }


    render(){
        return(
            <div  className="aboutme-page">
                <Tabs activeTab={this.state.activeTab} onChange={(tabId) => this.setState({ activeTab: tabId })} ripple>
                    <Tab>Color sport</Tab>
                    <Tab>Thai Language Day</Tab>
                    <Tab>Lab Sci</Tab>
                </Tabs>
                <section >
                    <Grid>
                        <Cell col={12}>
                            <div className="aboutme-page">{this.toggleCategories()}
                            </div>
                            <Grid className= "home-gride">
                                <Cell col={6}>
                                    <div className="banner-text">
                                        <p>if you want to know me</p>
                                    </div>
                                    <Button raised accent style={{marginRight:'15px'}}>
                                        <Navigation>
                                            <Link style={{color:'white'}}to="/">Home</Link>
                                        </Navigation>
                                    </Button>

                                    <Button raised accent >
                                        <Navigation>
                                            <Link style={{color:'white'}} to="/aboutme">About me</Link>
                                        </Navigation>
                                    </Button>
                                    <Button raised accent style={{marginLeft:'15px'}}>
                                        <Navigation >
                                            <Link style={{color:'white'}} to="/contact">Contact</Link>
                                        </Navigation>
                                    </Button>
                                </Cell>
                            </Grid>
                        </Cell>
                    </Grid>
                </section>
            </div>

        )
    }
}
export default Activity;