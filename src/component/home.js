import React,{Component} from 'react'
import {Grid, Cell, Button, Navigation,Tooltip,Icon} from 'react-mdl'
import {Link} from "react-router-dom";
class Home extends Component{
    render(){
        return(
            <div>
                <Grid className="header-color" >
                    <Cell col={6}>
                        <div className="banner-text">
                            <p>I' M   A Y E</p>
                            <p>SIRAPHAT   PURANAWONG</p>
                            <p>Welcome to my Home</p>
                        </div>
                    </Cell>
                    <Cell col={6}>
                        <img
                            src="https://scontent.fbkk1-1.fna.fbcdn.net/v/t1.0-9/37979116_1057799797720518_2951097476762828800_n.jpg?_nc_cat=104&_nc_eui2=AeEOpJ72KQ3Citzvdf7Ni8LK00Ft2FKLS31wIPkfhTRL7J0OI-VTkXbUZDQ6o_8B7t1QDz_RsSh2d5-uBS0t_cCfrv4aIBnuoJgCUD6797zuzw&_nc_ht=scontent.fbkk1-1.fna&oh=e0ae27e86818173f7977a318ff9f87c1&oe=5C406ACB"
                            alt="avatar"
                            className="avatar-img"
                        />
                    </Cell>
                </Grid>
                <Grid className="home-gride" >
                    <Cell col={6}>
                        <div className="banner-text">
                            <h2>Talk with me</h2>
                        </div>
                        <img
                            src="https://scontent.fbkk5-7.fna.fbcdn.net/v/t1.0-9/37826061_1053966914770473_909786541306413056_n.jpg?_nc_cat=108&_nc_eui2=AeHeBN3ZR3rtRHHKe7EQfP9d1w7v3GHeGzJ9wmx_XyBcyCsr5JEge8ZSvGZP0qkvDe6PQRXohgQcAwneGnwYkJtYYZU3VjKAF-5hW7CVrnFLLQ&_nc_ht=scontent.fbkk5-7.fna&oh=a9646d3e67f38bd39e86323c5a474022&oe=5C7C6307"
                            alt="avatar"
                            style={{height:'300px'}}
                            className="avatar-img"
                        />
                        <div className="banner-text" >
                            <h1>This is the first time i started coding for introduce myself.</h1>
                            <h1>So i'll tell you about my story,the hobby of the likes and much more</h1>
                            <h1>i'm glad you come to my web :)</h1>
                            <hr/>
                            <p>Do you want to know me?</p>
                        </div>
                    </Cell>
                </Grid>
                <Grid className= "home-gride">
                    <Cell col={6}>
                        <div>
                            <h3>
                                My hobby
                            </h3>
                        </div>
                        <img
                            src="https://i.pinimg.com/originals/9c/20/07/9c2007b78f793e3b1225590ae7527c80.jpg"
                            alt="avatar"
                            style={{height:'300px'}}
                            className="avatar-img"
                        />
                    </Cell>
                    <Cell col={6}>
                        <div className="banner-text" style={{marginTop:'100px'}}>
                            <h1>Someday i wacthed anime all the day or the free day.</h1>
                            <h1>My favorite anime is Doramon but there are many anime that i wachthed  </h1>
                            <h1>i wacthed the most romance anime and thi is my anime that i wacthed</h1>
                            <li>Kaichou wa maid sama</li>
                            <li>Doremon(Everyday)</li>
                            <li>One piece</li>
                            <li>Cardcaptor Sakura</li>
                            <li>etc.</li>
                        </div>
                    </Cell>
                </Grid>
                <Grid className="home-gride" >
                    <Cell col={6}>
                        <div className="banner-text" style={{marginTop:'100px'}} >
                            <h1>When i go home i will play with my cat.</h1>
                            <h1>in my house have about 11 cat. i love it all.</h1>
                            <h1>i admit that i am a cat slave.Raise all myheart to the cats</h1>
                            <h1>i love it!</h1>
                            <p>Do you love it too?</p>
                        </div>
                    </Cell>
                    <Cell col={6}>
                        <div className="banner-text" >
                            <p>My Love</p>
                        </div>
                        <img
                            src="https://i.pinimg.com/originals/41/12/13/41121363ac4ff734b97d440750154d4c.jpg"
                            alt="avatar"
                            className="avatar-img"
                        />
                    </Cell>
                </Grid>
                <Grid className= "home-gride" >
                    <Cell col={6}>
                        <div>
                            <h3>
                                It' me
                            </h3>
                        </div>
                        <img
                            src="https://i.pinimg.com/originals/20/bc/82/20bc823dca35a81e608c23372177cb8b.jpg"
                            alt="avatar"
                            style={{height:'300px'}}
                            className="avatar-img"
                        />
                    </Cell>
                    <Cell col={6}>
                        <div className="banner-text" style={{marginTop:'100px'}}>
                            <h1>I think i like a cat many ways.Sometime i act like a cat.</h1>
                            <h1>Somtimes i don't care about anything on this planet.But i', mine</h1>
                            <h1>i wacthed the most romance anime and thi is my anime that i wacthed</h1>
                            <h1>I look like hard to understand.right?</h1>
                            <h1>But i don't care (^ ^)</h1>
                        </div>
                    </Cell>
                </Grid>
                <Grid className= "home-gride">
                    <Cell col={6}>
                        <div className="banner-text">
                            <p>if you want to know me</p>
                        </div>
                        <Button raised accent style={{marginRight:'15px'}}>
                            <Navigation>
                                <Link style={{color:'white'}}to="/aboutme">About me</Link>
                            </Navigation>
                        </Button>

                        <Button raised accent >
                            <Navigation>
                                <Link style={{color:'white'}} to="/activity">Activity</Link>
                            </Navigation>
                        </Button>
                        <Button raised accent style={{marginLeft:'15px'}}>
                            <Navigation >
                                <Link style={{color:'white'}} to="/contact">Contact</Link>
                            </Navigation>
                        </Button>
                    </Cell>
                </Grid>
            </div>
        )
    }
}
export default Home;