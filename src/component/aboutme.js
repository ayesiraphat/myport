import React,{Component} from 'react'
import {Grid, Cell, Button, Navigation} from 'react-mdl'
import {Link} from "react-router-dom";

class Aboutme extends Component{
    render(){
        return(
            <div >
                <Grid className="aboutme-page" >
                    <Cell col={6}>
                        <div className="banner-text">
                            <h3>THIS IS MY PROFILE</h3>
                        </div>
                        <img
                            src="https://scontent.fbkk5-7.fna.fbcdn.net/v/t1.0-9/37826061_1053966914770473_909786541306413056_n.jpg?_nc_cat=108&_nc_eui2=AeHeBN3ZR3rtRHHKe7EQfP9d1w7v3GHeGzJ9wmx_XyBcyCsr5JEge8ZSvGZP0qkvDe6PQRXohgQcAwneGnwYkJtYYZU3VjKAF-5hW7CVrnFLLQ&_nc_ht=scontent.fbkk5-7.fna&oh=a9646d3e67f38bd39e86323c5a474022&oe=5C7C6307"
                            alt="avatar"
                            style={{height:'300px'}}
                            className="avatar-img"
                        />
                        <div className="banner-text" >
                            <h3>Are you ready to go my profile?</h3>
                        </div>
                    </Cell>
                </Grid>
                <Grid className="aboutme-page" >
                    <Cell col={6}>
                        <div className="banner-text">
                            <h3>My profile</h3>
                        </div>
                        <div className="banner-text" style={{marginLeft:'30%'}}>
                            <h4>Name : Siraphat puranawong</h4>
                            <h4>Nickname: Aye</h4>
                            <h4>Age: 18 Years</h4>
                            <h4>School: king mongkut's institute of technology ladkrabang</h4>
                            <h4>blood group: AB</h4>
                            <h4>HBD : 29 April 2000 Saturday</h4>
                        </div>
                    </Cell>
                    <Cell col={6}>
                        <img
                            src="http://5b0988e595225.cdn.sohucs.com/images/20180127/29f3407529a44250ac841c204f3fddcd.jpeg"
                            alt="avatar"
                            style={{height:'300px',marginTop:'15%'}}
                            className="avatar-img"

                        />
                    </Cell>
                </Grid>
                <Grid className="aboutme-pagepink" >
                    <Cell col={6}>
                        <div className="banner-text">
                            <h3>My Favorite</h3>
                        </div>
                        <img
                            src="http://image.dek-d.com/24/606409/106773282"
                            alt="avatar"
                            style={{height:'300px',marginTop:'15%'}}
                            className="avatar-img"

                        />
                    </Cell>
                    <Cell col={6}>
                        <div className="banner-text">
                            <h3>Novel</h3>
                        </div>
                        <div className="banner-text" style={{marginTop:'20%'}}>
                            <h4>My favorite novel is CUBIC. i fascinated this novel! </h4>
                            <h4>love in the love of Lin Lanser and Nak. Lin really is a man in my dream</h4>
                            <h4>Sometime i read this novel i almost want to get into a novle!</h4>
                            <h4>But in the movie made me very disappointed.So i read a novel only.</h4>
                            <h4> However,i love this novel! i love Lin Lanser!!</h4>
                        </div>
                    </Cell>
                </Grid>
                <Grid className="aboutme-pagepink" >
                    <Cell col={6}>
                        <div className="banner-text" style={{marginTop:'40%',marginLeft:'30%'}}>
                            <h4>I love wacthed anime so much.you don't even think that much.</h4>
                            <h4>and My favorite anime is Cardcaptor Sakura.!</h4>
                            <h4>i watched this anime since i was about the same age as Sakura.</h4>
                            <h4>Until i was older than Sakura</h4>
                        </div>

                    </Cell>
                    <Cell col={6}>
                        <div className="banner-text">
                            <h3>Anime</h3>
                        </div>
                        <img
                            src="https://i.pinimg.com/originals/e2/8a/4f/e28a4f261cacd5ffc16b3b8a9ef34a1e.jpg"
                            alt="avatar"
                            style={{height:'300px',marginTop:'15%'}}
                            className="avatar-img"
                        />
                    </Cell>
                </Grid>
                <Grid className="aboutme-pagepink" >
                    <Cell col={6}>
                        <img
                            src="http://1.bp.blogspot.com/-Ky2hyUkiVas/UcPuyfenUtI/AAAAAAAAIPw/E2Kq7jTDb2M/s1600/Lenka+3.jpg"
                            alt="avatar"
                            style={{height:'300px',marginTop:'15%'}}
                            className="avatar-img"

                        />
                    </Cell>
                    <Cell col={6}>
                        <div className="banner-text">
                            <h3>Singer</h3>
                        </div>
                        <div className="banner-text" style={{marginTop:'20%'}}>
                            <h4>Lenka is women in my heart. i fdon't know why i ilike her. </h4>
                            <h4>maybe because i listen to her foreign music is first song.</h4>
                            <h4>Hermusic has great meaning and charm.</h4>
                            <h4>So i love her song!</h4>
                        </div>
                    </Cell>
                </Grid>
                <Grid className= "aboutme-pagepink">
                <Cell col={6}>
                    <div className="banner-text">
                        <p>if you want to know me</p>
                    </div>
                    <Button raised accent style={{marginRight:'15px'}}>
                        <Navigation>
                            <Link style={{color:'white'}}to="/">Home</Link>
                        </Navigation>
                    </Button>

                    <Button raised accent >
                        <Navigation>
                            <Link style={{color:'white'}} to="/activity">Activity</Link>
                        </Navigation>
                    </Button>
                    <Button raised accent style={{marginLeft:'15px'}}>
                        <Navigation >
                            <Link style={{color:'white'}} to="/contact">Contact</Link>
                        </Navigation>
                    </Button>
                </Cell>
            </Grid>
            </div>
        )
    }
}
export default Aboutme;