import React,{Component} from 'react'
import {Grid, Cell, List, ListItem, ListItemContent, Textfield, Button, Navigation} from 'react-mdl'
import {Link} from "react-router-dom";

class Contact extends Component{
    render(){
        return(
            <div >
                <div className="banner-text">
                    <h3>My contact</h3>
                </div>
                <Grid className= "activity-grid">
                    <Cell col={6}>
                        <img
                            src="https://scontent.fbkk1-2.fna.fbcdn.net/v/t1.0-9/37864409_1055361757964322_1941215507147390976_o.jpg?_nc_cat=106&_nc_eui2=AeGvkJfgoRN4cMixOKs7C8XGQR_L6vIE2XutXVRQuOKQq1F6vQY_t9CKCZQkrSNykfdTW6wVCSAN3ECe6LBixLanOwG4ErcNWZug6i8un0GnmA&_nc_ht=scontent.fbkk1-2.fna&oh=bfe031a869beef152adb1c47221fe046&oe=5C895315"
                            style={{height:'500px',width:'300'}}

                        />

                    </Cell>
                    <Cell col={6}>
                        <div className="banner-text"><h3>SIRAPHAT  PURANAWONG</h3></div>
                        <div className="banner-text" style={{marginLeft:'20%'}}>
                                <h4>FB: Siraphat puranawong</h4>
                                <h4>Line: Shysiraphat</h4>
                                <h4>E-mail: ayesiraohat@gmail.com</h4>
                        </div>
                        <div className="banner-text"><h3>You Can comment to me</h3></div>
                        <Textfield
                            onChange={() => {}}
                            label="Text lines..."
                            rows={3}
                            style={{width: '600px',marginLeft:'10%'}}
                        />



                    </Cell>
                </Grid>
                <Grid className= "aboutme-pagepink">
                    <Cell col={6}>
                        <div className="banner-text">
                            <p>if you want to know me</p>
                        </div>
                        <Button raised accent style={{marginRight:'15px'}}>
                            <Navigation>
                                <Link style={{color:'white'}}to="/home">Home</Link>
                            </Navigation>
                        </Button>

                        <Button raised accent >
                            <Navigation>
                                <Link style={{color:'white'}} to="/activity">Activity</Link>
                            </Navigation>
                        </Button>
                        <Button raised accent style={{marginLeft:'15px'}}>
                            <Navigation >
                                <Link style={{color:'white'}} to="/contact">Contact</Link>
                            </Navigation>
                        </Button>
                    </Cell>
                </Grid>
            </div>
        )
    }
}
export default Contact;